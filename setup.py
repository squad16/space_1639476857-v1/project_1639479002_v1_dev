from setuptools import find_packages, setup

with open('version', 'r') as version_file:
    version = version_file.read().strip()

setup(
    name='project_1639479002_v1_dev',
    packages=find_packages(where='src', exclude=['tests']),
    package_dir={'': 'src'},
    version=version,
    description='andshav1',
    author='Po Po'
)
